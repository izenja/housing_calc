#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>
using namespace std;

struct Scenario {
    string name;
    double total = 0;

    bool rent;
    double price;
    double rentAndPropTax;
    double buyCostPercent = 0.003;  //land transfer
    double buyCostFlat = 300 + 400 + 1000 + 280;  //appraisal, inspection, lawyer, title insurance
    double sellCostPercent = 0.05;
    double sellCostFlat = 1500;
    double outfitting = 1000;  //a rough delta for house-only stuff like lawnmower
    double outfittingDiscrecionary = 0;  //account for some more discrecionary house-related initial costs (like garage stuff)
    double utilities = 250 * 12;  //power heat water
    double maintenance = 3000;
    double insurance = 650;

    double downPayment = 0.05;
    double interestRate = 0.035;
    double rateIncAt5yr = 0.01;
    int mortgageLength = 25;
    int sellYear = 5;
    double stockReturn = 0.09;
    double appreciation = 0.03;// 0.027;
    double expenseGrowth = 0.03;
    double cmhc = 0.038;

    Scenario(string name, double price, double annualFee, bool rent) :
        name(name), rent(rent), price(price), rentAndPropTax(annualFee) {
        if (rent) {
            price = 0;
            outfitting = 0;
            buyCostFlat = 200;
            buyCostPercent = 0;
            sellCostPercent = 0;
            sellCostFlat = 0;
            maintenance = 0;
            insurance = 20 * 12;
        }
    }

    double AnnuityPayment(double in, double curInterest, int remainingYears) {
        return in * (curInterest + (curInterest / (pow(1 + curInterest, remainingYears) - 1)));
    }

    double StockReturns(double input) {
        return input * stockReturn;
    }

    void Run(bool print) {
        // Initial
        auto down = price * downPayment;
        auto mortgage = price * (1 + cmhc) - down;
        total = down;
        total += price * buyCostPercent + buyCostFlat + outfitting + outfittingDiscrecionary;
        //auto stockTotal = total + StockReturns(total);

        auto principalBuilt = down;
        auto annuity = AnnuityPayment(mortgage, interestRate, mortgageLength);
        auto curFee = rentAndPropTax;

        double totalYearly = 0;
        double stockTotal = 0;
        double curMaint = maintenance;
        double curInterest = interestRate;
        for (int year = 0; year < sellYear; year++) {
            if (year == 5) {
                curInterest += rateIncAt5yr;
                annuity = AnnuityPayment(mortgage, curInterest, mortgageLength - year);
            }

            auto yearTotal = annuity + curFee + utilities + curMaint + insurance;
            total += yearTotal;
            totalYearly += yearTotal;

            auto interest = mortgage * curInterest;
            auto princip = annuity - interest;
            principalBuilt += princip;
            principalBuilt += price * appreciation;
            curFee *= 1 + (expenseGrowth + appreciation) / 2.0;
            curMaint *= expenseGrowth;
            utilities *= expenseGrowth;
            mortgage -= princip;
            stockTotal += StockReturns(total);
        }

        // Final
        // NOTE: stock opporunity cost doesn't reflect subsequent gains loss
        auto sellCost = price * sellCostPercent + sellCostFlat;
        principalBuilt -= price * cmhc;  //cmhc was added to mortgage total but it's not recovered in sale
        total += sellCost;
        stockTotal += StockReturns(sellCost);
        total += stockTotal;
        total -= principalBuilt;

        if (print) {
            cout << name << " (yr " << sellYear << " sell)"  << endl;
            cout << "   " << "total cost = " << total << endl;
            cout << "   " << "avg monthly out-of-pocket = " << totalYearly  / sellYear / 12.0 << endl;
            cout << "   " << "principal built = " << principalBuilt << endl;
            cout << "   " << "stocks opportunity cost = " << stockTotal << endl;
        }
    }
};

int main() {
    vector<Scenario> scenarios;

    scenarios.push_back({"280k house", 280000, 2500, false});
    scenarios.back().outfittingDiscrecionary = 500;

    scenarios.push_back({"380k house", 380000, 3080, false});
    scenarios.back().appreciation += 0.005;  //likely appreciates a bit better than a cheap one
    scenarios.back().outfittingDiscrecionary = 1000;

    scenarios.push_back({"250k condo", 250000, 2700 + 380 * 12, false});
    scenarios.back().utilities = 80 * 12;
    scenarios.back().maintenance = 250;

    scenarios.push_back({"275k townhouse", 275000, 2700 + 300 * 12, false});
    scenarios.back().utilities -= 40 * 12;
    scenarios.back().maintenance -= 500;
    scenarios.back().outfittingDiscrecionary = 500;

    scenarios.push_back({"325k townhouse", 325000, 2700 + 180 * 12, false});
    scenarios.back().utilities -= 20 * 12;
    scenarios.back().maintenance -= 500;
    scenarios.back().outfittingDiscrecionary = 500;

    scenarios.push_back({"1100/month apt", 0, 1100 * 12, true});
    scenarios.back().utilities = 80 * 12;

    scenarios.push_back({"1400/month apt", 0, 1400 * 12, true});
    scenarios.back().utilities = 80 * 12;

    scenarios.push_back({"425k ottawa", 425000, 4500 - 10000, false});
    scenarios.back().outfittingDiscrecionary = 1000;
    scenarios.back().appreciation = 0.06;

    for (auto s : scenarios) {
        s.Run(true);
    }

    ofstream file("table.csv");
    for (auto& s : scenarios) {
        file << s.name << ",";
        for (int years = 1; years <= 25; years++) {
            s.sellYear = years;
            s.Run(false);
            file << s.total << ",";
        }
        file << endl;
    }

    return 0;
}
